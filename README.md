# README #

### What is this repository for? ###

BETA SOFTWARE -- USE AT YOUR OWN RISK

* dir2indexhtml Will produce the contents of an index.html for the current directory.
* Version: 1.0 BETA
* Copyright 2020 University of California

### How do I run this? ###

Change directory into your web directory, example: 

$ cd ~/public_html

FIRST, MAKE A COPY OF YOUR index.html FILE IF IT EXISTS (AND IF YOU WANT IT)

$ cp index.html index.html-ORIGINAL1

Then run:

$ perl dir2indexhtml > /tmp/index.html

When it's done, edit the /tmp/index.html to make sure it's not sharing files you don't want to share. Then copy it back into your directory.

$ cp /tmp/index.html ~/public_html/index.html 

### Contribution guidelines ###

Please use this as you see fit. Let me know if you make some signifigant improvements.

### Who do I talk to? ###

Ken Gribble: kwgribble@ucdavis.edu

BETA SOFTWARE -- USE AT YOUR OWN RISK
